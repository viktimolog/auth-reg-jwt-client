import React from 'react';
import img from 'loading_spinner.gif'

const Loading = () => (
    <div style={{ marginTop: '5%', textAlign: 'center' }}>
        <img src={img} alt='loading' />
        <h1>LOADING</h1>
    </div>
);

export default Loading;


