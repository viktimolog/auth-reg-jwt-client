import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { signOut } from 'store/auth/actions';

import 'index.css';

class DashboardView extends React.Component {
    static propTypes = {
        signOut: PropTypes.func.isRequired,
        account: PropTypes.object
    };
    render() {
        const { account, signOut } = this.props;
        return (
            <div className='login'>
                <div>Welcome {account.email}.</div>
                <div>Press Exit for sign out.</div>
                <div><input
                    type='submit'
                    value='Exit'
                    className='button'
                    onClick={signOut}
                />
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    account: state.authReducer.account
});

const mapDispatchToProps = dispatch => ({
    signOut: () => dispatch(signOut())
});

export default connect(mapStateToProps, mapDispatchToProps)(DashboardView);


