import React from 'react';
import { withRouter } from 'react-router-dom';
import 'index.css';

import Alert from 'components/modals/Alert';

class RecoveryPassword extends React.Component {
    state = {
        password: '',
        password2: '',
        alertShow: false
    }

    handleSubmit = (e) => {
        e.preventDefault();
        const { password, password2 } = this.state;
        if (password !== password2) {
            return this.handleAlertShow();
        }

        const credentials = {
            password: password.trim(),
            recoveryPasswordKey: this.props.recoveryPasswordKey
        };
        this.props.restorePassword(credentials);
    }

    handleAlertShow = () => this.setState({ alertShow: !this.state.alertShow });
    handleChangePassword = e => this.setState({ password: e.target.value });
    handleChangePassword2 = e => this.setState({ password2: e.target.value });

    handleMessageOk = () => {
        this.props.resetMessage();
        this.props.history.push('/sign-in');
    }

    render() {
        const { resetError, isError, isMessage } = this.props;

        return (
            <div className='login'>
                <Alert
                    visible={!!isError}
                    onClose={resetError}
                    text={isError}
                    type={'error'}
                />
                <Alert
                    visible={!!isMessage}
                    onClose={this.handleMessageOk}
                    text={isMessage}
                    type={'message'}
                />
                <Alert
                    visible={this.state.alertShow}
                    onClose={this.handleAlertShow}
                    text={'Passwords do not match!'}
                    type={'error'}
                />
                <form onSubmit={this.handleSubmit}>
                    <p>
                        <span className='fontawesome-lock'></span>
                        <input
                            type='password'
                            placeholder='Please input new password'
                            value={this.state.password}
                            onChange={this.handleChangePassword} />
                    </p>
                    <p>
                        <span className='fontawesome-lock'></span>
                        <input
                            type='password'
                            placeholder='Please repeat your password'
                            value={this.state.password2}
                            onChange={this.handleChangePassword2} />
                    </p>
                    <p><input type='submit' value='Confirm' /></p>
                </form>
            </div>
        );
    }
}

export default withRouter(RecoveryPassword);

