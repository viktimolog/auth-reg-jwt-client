import React from 'react';
import { withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { restorePassword, resetError, resetMessage, checkRecoveryPasswordKey } from 'store/auth/actions';

import RecoveryPassword from 'components/forms/RecoveryPassword';
import Alert from 'components/modals/Alert';

class RecoveryPasswordView extends React.Component {
    static propTypes = {
        isError: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.bool
        ]).isRequired,
        isMessage: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.bool
        ]).isRequired,
        restorePassword: PropTypes.func.isRequired,
        resetError: PropTypes.func.isRequired,
        checkRecoveryPasswordKey: PropTypes.func.isRequired,
        resetMessage: PropTypes.func.isRequired
    };

    handleErrorOk = () => {
        const { resetError, history, isCheckRecoveryPasswordKey } = this.props;
        resetError();
        if (!isCheckRecoveryPasswordKey) {
            history.push('/sign-in');
        }
    }

    render() {
        const { restorePassword, isError, resetError, resetMessage, isMessage, isCheckRecoveryPasswordKey, checkRecoveryPasswordKey } = this.props;

        if (isError) {
            return (
                <Alert
                    visible={!!isError}
                    onClose={this.handleErrorOk}
                    text={isError}
                    type={'error'}
                />
            );
        }

        if (!isCheckRecoveryPasswordKey) {
            checkRecoveryPasswordKey({ recoveryPasswordKey: this.props.match.params.recoveryPasswordKey });
        }

        return (
            <RecoveryPassword
                resetMessage={resetMessage}
                resetError={resetError}
                restorePassword={restorePassword}
                isError={isError}
                isMessage={isMessage}
                recoveryPasswordKey={this.props.match.params.recoveryPasswordKey}
            />
        );
    }
}

const mapStateToProps = state => ({
    isMessage: state.authReducer.isMessage,
    isError: state.authReducer.isError,
    isCheckRecoveryPasswordKey: state.authReducer.isCheckRecoveryPasswordKey
});

const mapDispatchToProps = dispatch => bindActionCreators({ restorePassword, resetError, resetMessage, checkRecoveryPasswordKey }, dispatch);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(RecoveryPasswordView));