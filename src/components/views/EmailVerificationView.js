import React, { Fragment } from 'react';
import { withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { verify, resetError, resetMessage } from 'store/auth/actions';

import Alert from 'components/modals/Alert';

class EmailVerificationView extends React.Component {
    static propTypes = {
        isError: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.bool
        ]).isRequired,
        isMessage: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.bool
        ]).isRequired,
        verify: PropTypes.func.isRequired,
        resetError: PropTypes.func.isRequired,
        resetMessage: PropTypes.func.isRequired,
        account: PropTypes.object
    };

    handleMessageOk = () => {
        const { account, history, resetMessage } = this.props;
        resetMessage();
        if (account) {
            history.push('/dashboard');
            return null;
        }
        history.push('/sign-in');
        return null;
    }

    handleErrorOk = () => {
        const { resetError, account, history, } = this.props;
        resetError();
        if (account) {
            history.push('/verification-info');
            return null;
        }
        history.push('/sign-in');
        return null;
    }

    render() {
        const { verify, isError, isMessage } = this.props;

        if (!isError && !isMessage) {
            verify({ verificationKey: this.props.match.params.verificationKey });
        }

        return (
            <Fragment>
                <Alert
                    visible={!!isMessage}
                    onClose={this.handleMessageOk}
                    text={isMessage}
                    type={'message'}
                />

                <Alert
                    visible={!!isError}
                    onClose={this.handleErrorOk}
                    text={isError}
                    type={'error'}
                />
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    isError: state.authReducer.isError,
    isMessage: state.authReducer.isMessage,
    account: state.authReducer.account
});

const mapDispatchToProps = dispatch => bindActionCreators({ verify, resetError, resetMessage }, dispatch);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(EmailVerificationView));
