import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { resetMessage, resetError, forgotPassword } from 'store/auth/actions';

import ForgotPassword from 'components/forms/ForgotPassword';

class ForgotPasswordView extends React.Component {
    static propTypes = {
        isError: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.bool
        ]).isRequired,
        isMessage: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.bool
        ]).isRequired,
        forgotPassword: PropTypes.func.isRequired,
        resetError: PropTypes.func.isRequired
    };

    render() {
        const { resetMessage, isError, resetError, isMessage, forgotPassword } = this.props;

        return (
            <ForgotPassword
                resetError={resetError}
                resetMessage={resetMessage}
                forgotPassword={forgotPassword}
                isError={isError}
                isMessage={isMessage}
            />
        );
    }
}

const mapStateToProps = state => ({
    isMessage: state.authReducer.isMessage,
    isError: state.authReducer.isError
});

const mapDispatchToProps = dispatch => bindActionCreators({ resetMessage, resetError, forgotPassword }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ForgotPasswordView);