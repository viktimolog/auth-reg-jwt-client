import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { resendLetter, resetError, resetMessage, signOut } from 'store/auth/actions';

import Alert from 'components/modals/Alert';
import 'index.css';

class VerificationInfoView extends React.Component {
    static propTypes = {
        isMessage: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.bool
        ]).isRequired,
        isError: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.bool
        ]).isRequired,
        resendLetter: PropTypes.func.isRequired,
        resetMessage: PropTypes.func.isRequired,
        signOut: PropTypes.func.isRequired,
        account: PropTypes.object
    };

    render() {
        const { resetMessage, resetError, isError, isMessage, account, resendLetter, signOut } = this.props;
        return (
            <div className='login' >
                <Alert
                    visible={!!isError}
                    onClose={resetError}
                    text={isError}
                    type={'error'}
                />
                <Alert
                    visible={!!isMessage}
                    onClose={resetMessage}
                    text={isMessage}
                    type={'message'}
                />
                <div>Welcome {account.email}.</div>
                <div>You have not passed the email verification.</div>
                <div>Please, read the last letter from us.</div>
                <div>Or press Resend letter for the new letter.</div>
                <div>
                    <input
                        type='submit'
                        value='Resend letter'
                        className='button'
                        onClick={() => resendLetter(account.userId)}
                    />
                </div>
                <div>
                    <input
                        type='submit'
                        value='Exit'
                        className='button'
                        onClick={signOut}
                    />
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    account: state.authReducer.account,
    isMessage: state.authReducer.isMessage,
    isError: state.authReducer.isError
});

const mapDispatchToProps = dispatch => ({
    resendLetter: id => dispatch(resendLetter(id)),
    resetError: () => dispatch(resetError()),
    resetMessage: () => dispatch(resetMessage()),
    signOut: () => dispatch(signOut())
})

export default connect(mapStateToProps, mapDispatchToProps)(VerificationInfoView);
