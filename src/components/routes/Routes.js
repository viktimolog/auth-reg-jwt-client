import React from 'react';
import { connect } from 'react-redux';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import SignUpView from 'components/views/SignUpView';
import SignInView from 'components/views/SignInView';
import DashboardView from 'components/views/DashboardView';
import EmailVerificationView from 'components/views/EmailVerificationView';
import VerificationInfoView from 'components/views/VerificationInfoView';
import RecoveryPasswordView from 'components/views/RecoveryPasswordView';
import ForgotPasswordView from 'components/views/ForgotPasswordView';

import GuardedRoute from 'components/routes/GuardedRoute';
import Loading from 'components/views/LoadingView';

const Routes = ({ isLoading }) =>
    isLoading
        ? <Loading />
        : <BrowserRouter>
            <Switch>
                <GuardedRoute exact={true} path={'/'} component={DashboardView} guest={false} verified={true} />

                <GuardedRoute exact={true} path={'/dashboard'} component={DashboardView} guest={false} verified={true} />

                <GuardedRoute exact={true} path={'/sign-in'} component={SignInView} guest={true} verified={false} />

                <GuardedRoute exact={true} path={'/sign-up'} component={SignUpView} guest={true} verified={false} />

                <GuardedRoute exact={true} path={'/verification-info'} component={VerificationInfoView} guest={false} verified={false} />

                <GuardedRoute exact={true} path={'/forgot-password'} component={ForgotPasswordView} guest={true} verified={false} />

                <GuardedRoute path={'/restore-password/:recoveryPasswordKey'} component={RecoveryPasswordView} guest={true} verified={false} />

                <Route path={'/email-verification/:verificationKey'} component={EmailVerificationView} />
            </Switch>
        </BrowserRouter>

const mapStateToProps = state => ({ isLoading: state.authReducer.isLoading });

export default connect(mapStateToProps, null)(Routes);



