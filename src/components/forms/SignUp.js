import React from 'react';
import { Link } from 'react-router-dom';
import 'index.css';

import Alert from 'components/modals/Alert';

class SignUp extends React.Component {
    state = {
        email: '',
        password: '',
        password2: '',
        alertShow: false
    }

    handleSubmit = e => {
        e.preventDefault();
        const { email, password, password2 } = this.state;

        if (password !== password2) {
            return this.handleAlertShow();
        }

        const credentials = {
            email: email.trim(),
            password: password.trim()
        };
        this.props.signUp(credentials)
    }

    handleAlertShow = () => this.setState({ alertShow: !this.state.alertShow });
    handleChangeEmail = e => this.setState({ email: e.target.value });
    handleChangePassword = e => this.setState({ password: e.target.value });
    handleChangePassword2 = e => this.setState({ password2: e.target.value });

    render() {
        const { isError, resetError } = this.props;

        return (
            <div className='login'>
                <Alert
                    visible={!!isError}
                    onClose={resetError}
                    text={isError}
                    type={'error'}
                />
                <Alert
                    visible={this.state.alertShow}
                    onClose={this.handleAlertShow}
                    text={'Passwords do not match!'}
                    type={'error'}
                />
                <form onSubmit={this.handleSubmit}>
                    <p>
                        <span className='fontawesome-user'></span>
                        <input
                            type='text'
                            placeholder='Please input your email'
                            value={this.state.email}
                            onChange={this.handleChangeEmail} />
                    </p>
                    <p>
                        <span className='fontawesome-lock'></span>
                        <input
                            type='password'
                            placeholder='Please input your password'
                            value={this.state.password}
                            onChange={this.handleChangePassword} />
                    </p>
                    <p>
                        <span className='fontawesome-lock'></span>
                        <input
                            type='password'
                            placeholder='Please repeat your password'
                            value={this.state.password2}
                            onChange={this.handleChangePassword2} />
                    </p>
                    <p><input type='submit' value='Sign Up' /></p>
                </form>
                <div>Or <Link to='/sign-in'>  already have an account? sign-in</Link></div>
                <div>Or <Link to='/forgot-password'> forgot password?</Link></div>
            </div>
        );
    }
}

export default SignUp; 