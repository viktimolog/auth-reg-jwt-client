import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { signUp, resetError } from 'store/auth/actions';

import SignUp from 'components/forms/SignUp';

class SignUpView extends React.Component {
    static propTypes = {
        isError: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.bool
        ]).isRequired,
        signUp: PropTypes.func.isRequired
    };

    render() {
        const { signUp, isError, resetError } = this.props;

        return (
            <SignUp
                signUp={signUp}
                isError={isError}
                resetError={resetError}
            />
        );
    }
}

const mapStateToProps = state => ({
    isError: state.authReducer.isError
});

const mapDispatchToProps = dispatch => bindActionCreators({ signUp, resetError }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(SignUpView);