import {
    RESTORE_SESSION,
    SIGN_IN,
    SIGN_OUT,
    SIGN_UP,
    ERROR_TRUE,
    ERROR_FALSE,
    MESSAGE_TRUE,
    MESSAGE_FALSE,
    VERIFICATION_TRUE,
    ERROR_TRUE_CATCH,
    LOADER_TRUE,
    CHECKING_RECOVERY_PASSWORD_KEY_TRUE
} from './actions';

const initialState = {
    account: null,
    isError: false,
    isMessage: false,
    isLoading: false,
    isCheckRecoveryPasswordKey: false
};

const authReducer = (state = initialState, action) => {
    switch (action.type) {

        case LOADER_TRUE: {
            return {
                ...state,
                isLoading: true
            }
        }

        case CHECKING_RECOVERY_PASSWORD_KEY_TRUE: {
            return {
                ...state,
                isCheckRecoveryPasswordKey: true,
                isLoading: false
            }
        }

        case RESTORE_SESSION: {
            return {
                ...state,
                account: action.payload.account,
                isLoading: false
            }
        }

        case SIGN_OUT: {
            return {
                ...state,
                account: null,
                isError: false,
                isMessage: false,
                isLoading: false
            };
        }

        case SIGN_UP: {
            return {
                ...state,
                account: action.payload.account,
                isLoading: false
            };
        }

        case VERIFICATION_TRUE: {
            let updateAccount = null;
            if (!!state.account) {
                updateAccount = { ...state.account };
                updateAccount.isVerified = true;
            }
            return {
                ...state,
                isMessage: action.payload.message,
                isLoading: false,
                account: updateAccount
            };
        }

        case MESSAGE_TRUE: {
            return {
                ...state,
                isMessage: action.payload.message,
                isLoading: false
            };
        }

        case MESSAGE_FALSE: {
            return {
                ...state,
                isMessage: false,
                isLoading: false
            };
        }

        case SIGN_IN: {
            return {
                ...state,
                account: action.payload.account,
                isLoading: false
            };
        }

        case ERROR_TRUE: {
            return {
                ...state,
                isError: action.payload.errors[0].message,
                isLoading: false
            };
        }

        case ERROR_TRUE_CATCH: {
            return {
                ...state,
                isError: action.payload.error,
                isLoading: false
            };
        }

        case ERROR_FALSE: {
            return {
                ...state,
                isError: false,
                isLoading: false
            };
        }

        default:
            return state;
    }
}

export default authReducer;