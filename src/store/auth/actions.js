import axios from 'axios';
import { axiosController } from 'utils/AxiosController';

export const LOADER_TRUE = 'LOADER_TRUE';
export const ERROR_TRUE = 'ERROR_TRUE';
export const ERROR_TRUE_CATCH = 'ERROR_TRUE_CATCH';
export const SIGN_UP = 'SIGN_UP';
export const signUp = credentials =>
    dispatch => {
        axios.post('users/register', credentials)
            .then(response => {
                if (!response.data.success) {
                    return dispatch({
                        type: ERROR_TRUE,
                        payload: response.data.payload
                    });
                }
                axiosController.saveToken(response.data.payload.token);
                return dispatch({
                    type: SIGN_UP,
                    payload: response.data.payload
                })
            })
            .catch(error => dispatch({
                type: ERROR_TRUE_CATCH,
                payload: {
                    error: error.toString()
                }
            }))
        return dispatch({
            type: LOADER_TRUE
        })
    };

export const VERIFICATION_TRUE = 'VERIFICATION_TRUE';
export const verify = verificationKey =>
    dispatch => {
        axios.post('users/verify', verificationKey)
            .then(response => {
                if (!response.data.success) {
                    return dispatch({
                        type: ERROR_TRUE,
                        payload: response.data.payload
                    });
                }
                return dispatch({
                    type: VERIFICATION_TRUE,
                    payload: response.data.payload
                })
            })
            .catch(error => dispatch({
                type: ERROR_TRUE_CATCH,
                payload: {
                    error: error.toString()
                }
            }))
        return dispatch({
            type: LOADER_TRUE
        })
    };

export const SIGN_IN = 'SIGN_IN';
export const signIn = credentials =>
    dispatch => {
        axios.post('users/login', credentials)
            .then(response => {
                if (!response.data.success) {
                    return dispatch({
                        type: ERROR_TRUE,
                        payload: response.data.payload
                    });
                }
                axiosController.saveToken(response.data.payload.token);
                return dispatch({
                    type: SIGN_IN,
                    payload: response.data.payload
                })
            })
            .catch(error => dispatch({
                type: ERROR_TRUE_CATCH,
                payload: {
                    error: error.toString()
                }
            }))
        return dispatch({
            type: LOADER_TRUE
        })
    };

export const RESTORE_SESSION = 'RESTORE_SESSION';
export const restoreSession = () =>
    dispatch => {
        if (!axiosController.isGetToken()) {
            return dispatch({
                type: RESTORE_SESSION,
                payload: {
                    account: null
                }
            })
        }
        axios.post('users/account')
            .then(response => {
                if (!response.data.success) {
                    axiosController.deleteToken();
                    return dispatch({
                        type: RESTORE_SESSION,
                        payload: {
                            account: null
                        }
                    })
                }
                return dispatch({
                    type: RESTORE_SESSION,
                    payload: response.data.payload
                })
            })
            .catch(error => dispatch({
                type: ERROR_TRUE_CATCH,
                payload: {
                    error: error.toString()
                }
            }))
        return dispatch({
            type: LOADER_TRUE
        })
    }

export const FORGOT_PASSWORD = 'FORGOT_PASSWORD';
export const forgotPassword = credentials =>
    dispatch => {
        axios.post('users/forgotPassword', credentials)
            .then(response => {
                if (!response.data.success) {
                    return dispatch({
                        type: ERROR_TRUE,
                        payload: response.data.payload
                    });
                }
                return dispatch({
                    type: MESSAGE_TRUE,
                    payload: response.data.payload
                })
            })
            .catch(error => dispatch({
                type: ERROR_TRUE_CATCH,
                payload: {
                    error: error.toString()
                }
            }))
        return dispatch({
            type: LOADER_TRUE
        })
    };

export const CHECKING_RECOVERY_PASSWORD_KEY_TRUE = 'CHECKING_RECOVERY_PASSWORD_KEY_TRUE';
export const checkRecoveryPasswordKey = checkRecoveryPasswordKey =>
    dispatch => {
        axios.post('users/checkRecoveryPasswordKey', checkRecoveryPasswordKey)
            .then(response => {
                if (!response.data.success) {
                    return dispatch({
                        type: ERROR_TRUE,
                        payload: response.data.payload
                    });
                }
                return dispatch({
                    type: CHECKING_RECOVERY_PASSWORD_KEY_TRUE
                })
            })
            .catch(error => dispatch({
                type: ERROR_TRUE_CATCH,
                payload: {
                    error: error.toString()
                }
            }))
        return dispatch({
            type: LOADER_TRUE
        })
    };

export const restorePassword = credentials =>
    dispatch => {
        axios.post('users/restorePassword', credentials)
            .then(response => {
                if (!response.data.success) {
                    return dispatch({
                        type: ERROR_TRUE,
                        payload: response.data.payload
                    });
                }
                return dispatch({
                    type: MESSAGE_TRUE,
                    payload: response.data.payload
                })
            })
            .catch(error => dispatch({
                type: ERROR_TRUE_CATCH,
                payload: {
                    error: error.toString()
                }
            }))
        return dispatch({
            type: LOADER_TRUE
        })
    };

export const MESSAGE_TRUE = 'MESSAGE_TRUE';
export const resendLetter = () =>
    dispatch => {
        axios.post('users/resendLetter')
            .then(response => {
                if (!response.data.success) {
                    return dispatch({
                        type: ERROR_TRUE,
                        payload: response.data.payload
                    });
                }
                return dispatch({
                    type: MESSAGE_TRUE,
                    payload: response.data.payload
                })
            })
            .catch(error => dispatch({
                type: ERROR_TRUE_CATCH,
                payload: {
                    error: error.toString()
                }
            }))
        return dispatch({
            type: LOADER_TRUE
        })
    };

export const ERROR_FALSE = 'ERROR_FALSE';
export const resetError = () => ({ type: ERROR_FALSE });

export const MESSAGE_FALSE = 'MESSAGE_FALSE';
export const resetMessage = () => ({ type: MESSAGE_FALSE });

export const SIGN_OUT = 'SIGN_OUT';
export const signOut = () => {
    axiosController.deleteToken();
    return { type: SIGN_OUT }
};

