import React from 'react';
import { bindActionCreators } from 'redux';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { restoreSession } from 'store/auth/actions';

class GuardedRoute extends React.Component {
    componentDidMount() {
        if (!this.props.account) {
            this.props.restoreSession();
        }
    }
    render() {
        const { component: Component, guest, verified, account, ...rest } = this.props;

        const isVerified = !!account ? !!account.isVerified : false;

        return <Route {...rest} render={
            props => {
                if (!guest === !!account && verified === isVerified) {
                    return <Component {...props} />
                }
                if (!account) {
                    return <Redirect to={'/sign-in'} />
                }
                if (isVerified) {
                    return <Redirect to={'/dashboard'} />
                }
                return <Redirect to={'/verification-info'} />
            }
        }
        />
    }
}

const mapStateToProps = state => ({
    account: state.authReducer.account
});

const mapDispatchToProps = dispatch => bindActionCreators({ restoreSession }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(GuardedRoute);

