import React from 'react';
import { Link } from 'react-router-dom';
import 'index.css';

import Alert from 'components/modals/Alert';

class ForgotPassword extends React.Component {
    state = { email: '' }

    handleSubmit = e => {
        e.preventDefault();
        const { email } = this.state;
        const credentials = {
            email: email.trim()
        };
        this.props.forgotPassword(credentials)
    }

    handleChangeEmail = e => this.setState({ email: e.target.value });

    render() {
        const { resetMessage, resetError, isError, isMessage } = this.props;

        return (
            <div className='login'>
                <Alert
                    visible={!!isError}
                    onClose={resetError}
                    text={isError}
                    type={'error'}
                />
                <Alert
                    visible={!!isMessage}
                    onClose={resetMessage}
                    text={isMessage}
                    type={'message'}
                />
                <form onSubmit={this.handleSubmit}>
                    <p>
                        <span className='fontawesome-user'></span>
                        <input
                            type='text'
                            placeholder='Please input your email'
                            value={this.state.email}
                            onChange={this.handleChangeEmail} />
                    </p>
                    <p><input type='submit' value='Reset password' /></p>
                </form>
                <div>Or <Link to='/sign-in'>  already have an account? sign-in</Link></div>
                <div>Or <Link to='/sign-up'>  register now!</Link></div>
            </div>
        );
    }
}

export default ForgotPassword;

