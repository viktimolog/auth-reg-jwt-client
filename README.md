Get Started

Clone the project git clone https://viktimolog@bitbucket.org/viktimolog/auth-reg-jwt-client.git

Change the directory cd auth-reg-jwt-client 

Install all dependencies npm install or yarn install

Run yarn start or npm start

Viewing the application in your browser http://localhost:3000
