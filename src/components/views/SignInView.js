import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { signIn, resetError } from 'store/auth/actions';

import SignIn from 'components/forms/SignIn';

class SignInView extends React.Component {
    static propTypes = {
        isError: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.bool
        ]).isRequired,
        signIn: PropTypes.func.isRequired,
        resetError: PropTypes.func.isRequired
    };
    render() {
        const { signIn, isError, resetError } = this.props;
        return (
            <SignIn
                signIn={signIn}
                isError={isError}
                resetError={resetError}
            />
        );
    }
}

const mapStateToProps = state => ({
    isError: state.authReducer.isError
});

const mapDispatchToProps = dispatch => bindActionCreators({ signIn, resetError }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(SignInView);