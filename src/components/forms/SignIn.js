import React from 'react';
import { Link } from 'react-router-dom';
import 'index.css';

import Alert from 'components/modals/Alert';

class SignIn extends React.Component {
    handleSubmit = e => {
        e.preventDefault(); 
        const credentials = {
            email: this.inputEmail.value.trim(),
            password: this.inputPassword.value.trim()
        };
        this.props.signIn(credentials)
    }

    render() {
        const { isError, resetError } = this.props;

        return (
            <div className='login'>
                <Alert
                    visible={!!isError}
                    onClose={resetError}
                    text={isError}
                    type={'error'}
                />
                <form onSubmit={this.handleSubmit}>
                    <p>
                        <span className='fontawesome-user'></span>
                        <input
                            type='text'
                            placeholder='Please input your email'
                            ref={ref => this.inputEmail = ref}
                        />
                    </p>
                    <p>
                        <span className='fontawesome-lock'></span>
                        <input
                            type='password'
                            placeholder='Please input your password'
                            ref={ref => this.inputPassword = ref}
                        />
                    </p>
                    <p><input type='submit' value='Log in' /></p>
                </form>
                <div>Or <Link to='/sign-up'> register now!</Link></div>
                <div>Or <Link to='/forgot-password'> forgot password?</Link></div>
            </div>
        );
    }
}

export default SignIn; 