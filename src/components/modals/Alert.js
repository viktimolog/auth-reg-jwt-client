import React from 'react';
import PropTypes from 'prop-types';
import Modal from 'react-modal';

Modal.setAppElement('#root');

class Alert extends React.Component {
    static propTypes = {
        text: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.bool
        ]).isRequired,
        visible: PropTypes.bool.isRequired,
        type: PropTypes.string.isRequired
    };

    render() {
        const messageTitleColor = 'blue';
        const errorTitleColor = 'brown';
        const messageTitle = 'This is an information message:';
        const errorTitle = 'This is an error message:';


        const color = this.props.type === 'message'
            ? messageTitleColor
            : this.props.type === 'error'
                ? errorTitleColor
                : null;

        const title = this.props.type === 'message'
            ? messageTitle
            : this.props.type === 'error'
                ? errorTitle
                : null;

        const buttonStyle = {
            position: 'absolute',
            bottom: 15,
            right: 15,
            width: '20%'
        }

        const titleAlertStyle = {
            fontSize: '16px',
            color: color
        }

        const iconStyle = {
            backgroundColor: color,
            borderRadius: 40,
            color: 'black',
            width: 40,
            height: 40,
            lineHeight: '40px',
            textAlign: 'center',
            marginRight: 20
        }

        const Icon = this.props.type === 'message'
            ? <span className='fontawesome-ok' style={iconStyle}></span>
            : this.props.type === 'error'
                ? <span style={iconStyle}>X</span>
                : null;

        return (
            <Modal
                isOpen={this.props.visible}
                className='alert'
            >
                <div style={{ display: 'flex', alignItems: 'center' }}>
                    {Icon}
                    <span style={titleAlertStyle}>{title}</span>
                </div>
                <div style={{ marginLeft: '20px' }}>{this.props.text}</div>
                <div>
                    <input
                        type='submit'
                        value='OK'
                        className='button'
                        onClick={this.props.onClose}
                        style={buttonStyle}
                    />
                </div>
            </Modal>
        );
    }
}

export default Alert;
