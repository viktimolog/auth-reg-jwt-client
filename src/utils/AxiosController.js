import axios from 'axios';
import baseUrl from 'config/baseUrl';

class AxiosController {
    constructor() {
        axios.defaults.baseURL = baseUrl;
    }

    isGetToken = () => {
        const token = localStorage.getItem('token');
        if (!token) return false;

        axios.defaults.headers = {
            Authorization: `Bearer ${token}`
        }
        return true;
    }

    deleteToken = () => localStorage.clear();

    saveToken = token => {
        localStorage.setItem('token', token);
        axios.defaults.headers = {
            Authorization: `Bearer ${token}`
        }
    }
}

export const axiosController = new AxiosController();        